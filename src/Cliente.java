import java.net.Socket;


public class Cliente extends Thread 
{

	private Socket sktCliente = null;
	private int numConsultas;
	private Buffer buffer;
	
	public Cliente(Socket pSocket, int numConsultasPorCliente) 
	{
		this.sktCliente = pSocket;
		numConsultas = numConsultasPorCliente;
	}
	
//	@Override
//	public void run() 
//	{
//		for (int i = 0; i < numConsultas; i++) 
//		{
//			Mensaje mensaje = new Mensaje(i+1);
//			enviarMensaje(mensaje);
//		}
//		// AVISAR AL BUFFER QUE SE RETIRA
//	}

	public void run(){
		for(int i = 0; i<numConsultas; i++){
			try{
				int n = (Integer) new Integer(i);
				Mensaje mensaje = new Mensaje(n);
				buffer.almacenar(n, mensaje);
				System.out.println("se envia mensaje al buffer: " + n);	
				
				if(numConsultas == 0){
					wait();
				}
				if(numConsultas != 0){
					notify();
				}
			}catch(Exception e){
				System.out.println(e);
			};
			buffer.retirar(numConsultas);
		}
	}

}
