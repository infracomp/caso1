
public class Buffer 
{

	private int capacidad;
	private Mensaje[] buffer;
	int cont;
	
	public Buffer() 
	{
		cont = 0;
		buffer = new Mensaje[50];
	}
	
	public synchronized void almacenar(int pos, Mensaje mns)
	{
		if(buffer[pos]!=null){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("La posici�n ya est� asignada");
		}
		else if(buffer[pos]==null){
			notify();
			buffer[pos] = mns;
			notify();
		}
		
	}
	
	public synchronized Mensaje retirar(int pos)
	{
		Mensaje rta = null;
		if(buffer[pos]==null){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("No existe elemento en esta posici�n");
		} else if(buffer[pos]!=null){
			rta = buffer[pos];
		}
		notify();
		return rta;
	}

}
