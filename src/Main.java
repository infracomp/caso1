import java.io.FileInputStream;
import java.util.Properties;

public class Main 
{

	private static String rutaArchivo;
	private static int numClientes;
	private static int numServidores;
	private static int numConsultasPorCliente;

	public static void main(String[] args) 
	{
		rutaArchivo = "./data/archivo.txt";
		System.out.println("-------------- SIMULADOR SISTEMA GDTS ----------------");
		System.out.println();
		try
		{
			Properties datos = new Properties( );
			FileInputStream in = new FileInputStream( rutaArchivo );
			datos.load( in );
			in.close( );
			
			//LEER LOS DATOS NECESARIOS PARA LA INICIALIZACIÓN
			numClientes = Integer.parseInt(datos.getProperty("numClientes"));
			numServidores = Integer.parseInt(datos.getProperty("numServidores"));
			numConsultasPorCliente = Integer.parseInt(datos.getProperty("numConsultasPorCliente"));
			
			System.out.println("Se cargó la información del archivo");
			System.out.println("Numero de clientes = " + numClientes);
			System.out.println("Numero de servidores = "+numServidores);
			System.out.println("Numero de consultas por cliente = " + numConsultasPorCliente);
			
			for (int i = 0; i < numClientes; i++) 
			{
				new Cliente(numConsultasPorCliente).start();
			}
			
			for (int i = 0; i < numServidores; i++) 
			{
				new Servidor().start();
			}
			
		}
		catch( Exception e )
		{
			System.out.println("Error");
		}
	}

}
